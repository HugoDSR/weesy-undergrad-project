package databaseClient;

/**
 * Created by Rui on 09/05/2016.
 */
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;

import java.io.File;


public class PostClient {
    String filepath;
    boolean correubem = true;

    public PostClient(String filepath) {
        this.filepath = filepath;
    }

    public boolean uploadFile(){

        Thread thread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

                    HttpPost httppost = new HttpPost("http://deti-pei-vm3.ua.pt:8080/file-upload-1.0.0-SNAPSHOT/api/upload");
                    File file = new File(filepath);

                    MultipartEntity mpEntity = new MultipartEntity();
                    ContentBody cbFile = new FileBody(file);
                    mpEntity.addPart("file", cbFile);


                    httppost.setEntity(mpEntity);
                    System.out.println("executing request " + httppost.getRequestLine());
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity resEntity = response.getEntity();

                    System.out.println(response.getStatusLine());
                    if (resEntity != null) {
                        Log.d("ERRO UPLOAD", "Aqui1");
                        System.out.println(EntityUtils.toString(resEntity));
                    }
                    if (resEntity != null) {
                        Log.d("ERRO UPLOAD", "Aqui2");

                        resEntity.consumeContent();
                    }

                    httpclient.getConnectionManager().shutdown();
                }catch (Exception e){
                    correubem = false;
                    Log.d("ERRO UPLOAD", e.getMessage());
                }
            }
        });

        thread.start();


        return correubem;

    }


}