package userinterface;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import github.bewantbe.audio_analyzer_for_android.AnalyzeActivity;
import github.bewantbe.audio_analyzer_for_android.AnalyzeActivityFromWav;
import github.bewantbe.audio_analyzer_for_android.R;
import userinterface.XMLParser.XMLParserGetPatient;

/**
 * Created by hugo on 10-04-2016.
 */
public class PatientData extends Activity implements SearchView.OnQueryTextListener {

    private String nomePatient="";
    public ArrayList<String> local;
    public ArrayList<String> tmpLocal;
    private List listPatientsShow;
    private ListView lvp;
    boolean redeUA = false;
    boolean stored = false;
    private Patient patient;
    private SearchView search;
    private int size;
    SharedPreferences mSharedPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dadospaciente);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            redeUA = extras.getBoolean("UA_DISPONIVEL");
            // and get whatever type user account id is
        }

        local = new ArrayList<String>();
        tmpLocal = new ArrayList<String>();
        populateList();
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(this);

        EditText editTextName = (EditText) findViewById(R.id.editNome);
        editTextName.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {

                boolean handle = false;
                nomePatient = textView.getText().toString();

                return handle;

            }
        });

        /*findViewById(R.id.buttonPlanosRecolha).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(PatientData.this, view);
                popupMenu.setOnMenuItemClickListener(PatientData.this);
                popupMenu.inflate(R.menu.planorecolha_popup);
                popupMenu.show();
            }
        });*/


        if(redeUA) {

             listPatientsShow = new ArrayList<String>(ListPacients.listPatients.values());


            lvp = (ListView) findViewById(R.id.listViewPatients);

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listPatientsShow);
            lvp.setAdapter(arrayAdapter);

            lvp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View v, int _position, long id) {
                    v.setSelected(true);
                    int patientID = ListPacients.listPatients.inverse().get(listPatientsShow.get(_position));

                    try {
                        XMLParserGetPatient pat = new XMLParserGetPatient(patientID);
                        patient = pat.execute().get();

                        nomePatient = patient.getNome();

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                }


            });
            search = (SearchView) findViewById(R.id.searchPatient);
            lvp.setTextFilterEnabled(true);
            setupSearchView();
        }
    }

    private void setupSearchView() {
        search.setIconifiedByDefault(false);
        search.setOnQueryTextListener(this);
        search.setSubmitButtonEnabled(true);
        search.setQueryHint("Search Here");
    }

    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            lvp.clearTextFilter();
        } else {
            lvp.setFilterText(newText.toString());
        }
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }



    public void createPlan(View view){
        Intent getOrder = new Intent(this, SelectSampleOrder.class);
        startActivityForResult(getOrder, 1);
    }

    public void loadSample(View view){
        if(!mSharedPreference.contains("Status_0")){
            Toast.makeText(this, "Can't load Preference", Toast.LENGTH_SHORT).show();
        }
        else{
            loadArray( );
            startIntent();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode ==1){
            if(resultCode == Activity.RESULT_OK){
                stored = true;
                this.tmpLocal = data.getStringArrayListExtra("result");
                saveArray();
                Toast.makeText(this, "Nova Ordem Aceite "+ local.size(), Toast.LENGTH_SHORT).show();
            }
            if(resultCode == Activity.RESULT_CANCELED){
                Toast.makeText(this, "Problema a Carregar Ordem Nova", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public boolean saveArray(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor mEdit1 = sp.edit();

        mEdit1.putInt("Status_size", tmpLocal.size());

        for(int i=0;i<tmpLocal.size();i++)
        {
            mEdit1.remove("Status_" + i);
            mEdit1.putString("Status_" + i, tmpLocal.get(i));
        }

        return mEdit1.commit();
    }

    public void loadArray(){
        Toast.makeText(this, "Loading Preferences", Toast.LENGTH_SHORT).show();

        local.clear();
        int size = mSharedPreference.getInt("Status_size", 0);

        for(int i=0;i<size;i++)
        {
            local.add(mSharedPreference.getString("Status_" + i, null));
        }

    }

    public String getNomePatient(){ return this.nomePatient;  }

    public ArrayList getlocal(){return this.local;}

    public void insertName(View view){
        this.nomePatient="";
    }

    public void populateList(){

        local.add("peito_pescoco");
        local.add("peito_superior_esquerdo");
        local.add("peito_superior_direito");
        local.add("peito_inferior_esquerdo");
        local.add("peito_inferior_direito");
        local.add("lateral_esquerdo");
        local.add("costas_superior_esquerdo");
        local.add("costas_superior_direito");
        local.add("costas_inferior_esquerdo");
        local.add("costas_inferior_direito");
        local.add("lateral_direito");
    }

    public void startSample(View view){

        if(nomePatient.equals("")){
            Toast toast = Toast.makeText(getApplicationContext(),"Erro: indique nome de paciente",Toast.LENGTH_SHORT);
            toast.show();
        }
        else{
            startIntent();
            Toast toast = Toast.makeText(getApplicationContext(),"Sucesso: "+getNomePatient().toString(),Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void startIntent(){
        //Intent intent = new Intent(PatientData.this, AnalyzeActivityFromWav.class);

        Intent intent = new Intent(PatientData.this, AnalyzeActivity.class);
        intent.putExtra("UA_DISPONIVEL", redeUA);
        intent.putExtra("EXTRA_NOMECLIENT", getNomePatient());
        intent.putExtra("EXTRA_LOCALLIST", getlocal());
        startActivity(intent);
    }

    public void procurarPaciente(View v) {
        LinearLayout layout = (LinearLayout) findViewById(R.id.layoutSearchPatient);
        layout.setVisibility(View.VISIBLE);
        LinearLayout layout2 = (LinearLayout) findViewById(R.id.layoutNewPatient);
        layout2.setVisibility(View.GONE);
        Button button = (Button) findViewById(R.id.buttonNovo);
        button.setVisibility(View.VISIBLE);
        Button button2 = (Button) findViewById(R.id.buttonProcurar);
        button2.setVisibility(View.GONE);
    }

    public void novoPaciente(View v) {
        LinearLayout layout = (LinearLayout) findViewById(R.id.layoutNewPatient);
        layout.setVisibility(View.VISIBLE);
        LinearLayout layout2 = (LinearLayout) findViewById(R.id.layoutSearchPatient);
        layout2.setVisibility(View.GONE);
        Button button = (Button) findViewById(R.id.buttonProcurar);
        button.setVisibility(View.VISIBLE);
        Button button2 = (Button) findViewById(R.id.buttonNovo);
        button2.setVisibility(View.GONE);
    }
}
