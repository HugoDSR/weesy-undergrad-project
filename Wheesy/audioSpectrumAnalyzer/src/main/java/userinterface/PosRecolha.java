package userinterface;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import databaseClient.PostClient;
import databaseClient.SecurityMoves;
import github.bewantbe.audio_analyzer_for_android.R;

public class PosRecolha extends Activity {

    String path_to_values;

    SecurityMoves securityMoves;

    boolean redeUA = true;
    String relativeDir = "/Recorder";
    String nomePacient ="";
    String nowStr="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menupospesquisa);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            path_to_values = extras.getString("DEVICE_PATH_TO_VALUES");
            redeUA = extras.getBoolean("UA_DISPONIVEL");
            nomePacient = extras.getString("PATIENT_NAME");

            try {
                securityMoves = new SecurityMoves(nomePacient);
            } catch (Exception e) {

                Log.d("ERRO", "Erro:"+e);
            }

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            nowStr = df.format(new Date());

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goPatientData(View view){
        Intent intent = new Intent(getApplicationContext(), Menu1.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void goCollectSample(View view){
        //Apagar gravaçoes previamente recolhidas
        File path = new File(Environment.getExternalStorageDirectory().getPath() + relativeDir+"/" +nomePacient+"/"+nowStr);
        if (path.isDirectory())
        {
            String[] children = path.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(path, children[i]).delete();
            }
        }
        Intent intent = new Intent(getApplicationContext(), PatientData.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void novaconsulta(View v){
        Intent intent = new Intent(PosRecolha.this, PatientData.class);
        startActivity(intent);
    }

    public void listSamples(View v){
        // Intent intent = new Intent(Menu1.this, DataList.class);
        //startActivity(intent);

        if(!isNetworkAvailable()){
            Toast toast = Toast.makeText(getApplicationContext(),"Internet não disponivel: Não é possivel aceder a exames",Toast.LENGTH_LONG);
            toast.show();
        }else {

            Intent intent = new Intent(PosRecolha.this, PatientData.class);
            startActivity(intent);
        }
    }

    public void naoimplementado(View v){
        Toast toast = Toast.makeText(getApplicationContext(),"Ainda nao foi implementado",Toast.LENGTH_LONG);
        toast.show();
    }

    public void sendData(View v) throws Exception {

        if (isNetworkAvailable()) {
            if (redeUA) {

                Log.d("Files", "Path: " + path_to_values);
                File f = new File(path_to_values);

                File file[] = f.listFiles();
                Log.d("Files", "Size: " + file.length);
                for (int i = 0; i < file.length; i++) {

                    Log.d("Files", "FileName:" + path_to_values + "/" + file[i].getName());

                    //byte[] newFile = securityMoves.encrypt(path_to_values + "/" + file[i].getName());

                    PostClient up = new PostClient(path_to_values + "/" + file[i].getName());

                    boolean correubem = up.uploadFile();

                    if (!correubem) {
                        Toast toast = Toast.makeText(this, "Ocorreu um problema. Não ocorreu o Upload.", Toast.LENGTH_LONG);
                        toast.show();
                    }

                }

                Toast toast = Toast.makeText(this, "Operação bem sucedida.", Toast.LENGTH_LONG);
                toast.show();
            }else{

                Toast toast = Toast.makeText(this, "Sem acesso à rede UA. Base de dados não acessivel.", Toast.LENGTH_LONG);
                toast.show();

            }
        }else{

            Toast toast = Toast.makeText(this, "Sem acesso Internet. Base de dados não acessivel.", Toast.LENGTH_LONG);
            toast.show();
            }
    }



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}