package userinterface;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import github.bewantbe.audio_analyzer_for_android.R;

/**
 * Created by hugo on 13-05-2016.
 */
public class SelectSampleOrder extends Activity{

    public ArrayList<String> orderdList;
    private final ArrayList<String> samples = new ArrayList<>();
    private int count;

    private int[] pos;
    private boolean[] selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sampleorder);
        initVals();
        orderdList = new ArrayList<String>();
        pos = new int[11];
        selected = new boolean[11];
        for(int i=0; i<11; i++){
            pos[i]=0;
            selected[i]=false;
        }
        count = 0;
    }

    public void initVals(){
        samples.add("peito_pescoco");
        samples.add("peito_superior_esquerdo");
        samples.add("peito_superior_direito");
        samples.add("peito_inferior_esquerdo");
        samples.add("peito_inferior_direito");
        samples.add("lateral_esquerdo");
        samples.add("costas_superior_esquerdo");
        samples.add("costas_superior_direito");
        samples.add("costas_inferior_esquerdo");
        samples.add("costas_inferior_direito");
        samples.add("lateral_direito");
    }
    public void selectLoc(View view){
        switch(view.getId()) {
            case R.id.so_peito_superior_esquerdo:
                if(!selected[0]){
                    count++;
                    pos[0] = count;
                    TextView txt=(TextView) findViewById(R.id.so_peito_superior_esquerdo_text);
                    txt.setText(Integer.toString(pos[0]));
                    selected[0]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_peito_superior_esquerdo_text);
                    txt.setText(" ");
                    count--;
                    selected[0]=false;
                    unselect(pos[0]);
                    pos[0]=0;
                }
                break;
            case R.id.so_peito_superior_direito:
                if(!selected[1]){
                    count++;
                    pos[1]=count;
                    TextView txt=(TextView) findViewById(R.id.so_peito_superior_direito_text);
                    txt.setText(Integer.toString(pos[1]));
                    selected[1]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_peito_superior_direito_text);
                    txt.setText(" ");
                    count--;
                    selected[1]=false;
                    unselect(pos[1]);
                    pos[1]=0;
                }
                break;
            case R.id.so_peito_inferior_esquerdo:
                if(!selected[2]){
                    count++;
                    pos[2]=count;
                    TextView txt=(TextView) findViewById(R.id.so_peito_inferior_esquerdo_text);
                    txt.setText(Integer.toString(pos[2]));
                    selected[2]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_peito_inferior_esquerdo_text);
                    txt.setText(" ");
                    count--;
                    selected[2]=false;
                    unselect(pos[2]);
                    pos[2]=0;
                }
                break;
            case R.id.so_peito_inferior_direito:
                if(!selected[3]){
                    count++;
                    pos[3]=count;
                    TextView txt=(TextView) findViewById(R.id.so_peito_inferior_direito_text);
                    txt.setText(Integer.toString(pos[3]));
                    selected[3]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_peito_inferior_direito_text);
                    txt.setText(" ");
                    count--;
                    selected[3]=false;
                    unselect(pos[3]);
                    pos[3]=0;
                }
                break;
            case R.id.so_peito_pescoco:
                if(!selected[4]){
                    count++;
                    pos[4]=count;
                    TextView txt=(TextView) findViewById(R.id.so_peito_pescoco_text);
                    txt.setText(Integer.toString(pos[4]));
                    selected[4]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_peito_pescoco_text);
                    txt.setText(" ");
                    count--;
                    selected[4]=false;
                    unselect(pos[4]);
                    pos[4]=0;
                }
                break;
            case R.id.so_lateral_esquerdo:
                if(!selected[5]){
                    count++;
                    pos[5]=count;
                    TextView txt=(TextView) findViewById(R.id.so_lateral_esquerdo_text);
                    txt.setText(Integer.toString(pos[5]));
                    selected[5]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_lateral_esquerdo_text);
                    txt.setText(" ");
                    count--;
                    selected[5]=false;
                    unselect(pos[5]);
                    pos[5]=0;
                }
                break;
            case R.id.so_costas_superior_esquerdo:
                if(!selected[6]){
                    count++;
                    pos[6]=count;
                    TextView txt=(TextView) findViewById(R.id.so_costas_superior_esquerdo_text);
                    txt.setText(Integer.toString(pos[6]));
                    selected[6]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_costas_superior_esquerdo_text);
                    txt.setText(" ");
                    count--;
                    selected[6]=false;
                    unselect(pos[6]);
                    pos[6]=0;
                }
                break;
            case R.id.so_costas_superior_direito:
                if(!selected[7]){
                    count++;
                    pos[7]=count;
                    TextView txt=(TextView) findViewById(R.id.so_costas_superior_direito_text);
                    txt.setText(Integer.toString(pos[7]));
                    selected[7]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_costas_superior_direito_text);
                    txt.setText(" ");
                    count--;
                    selected[7]=false;
                    unselect(pos[7]);
                    pos[7]=0;
                }
                break;
            case R.id.so_costas_inferior_esquerdo:
                if(!selected[8]){
                    count++;
                    pos[8]=count;
                    TextView txt=(TextView) findViewById(R.id.so_costas_inferior_esquerdo_text);
                    txt.setText(Integer.toString(pos[8]));
                    selected[8]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_costas_inferior_esquerdo_text);
                    txt.setText(" ");
                    count--;
                    selected[8]=false;
                    unselect(pos[8]);
                    pos[8]=0;

                }
                break;
            case R.id.so_costas_inferior_direito:
                if(!selected[9]){
                    count++;
                    pos[9]=count;
                    TextView txt=(TextView) findViewById(R.id.so_costas_inferior_direito_text);
                    txt.setText(Integer.toString(pos[9]));
                    selected[9]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_costas_inferior_direito_text);
                    txt.setText(" ");
                    count--;
                    selected[9]=false;
                    unselect(pos[9]);
                    pos[9]=0;
                }
                break;
            case R.id.so_lateral_direito:
                if(!selected[10]){
                    count++;
                    pos[10]=count;
                    TextView txt=(TextView) findViewById(R.id.so_lateral_direito_text);
                    txt.setText(Integer.toString(pos[10]));
                    selected[10]=true;
                }
                else{
                    TextView txt=(TextView) findViewById(R.id.so_lateral_direito_text);
                    txt.setText(" ");
                    count--;
                    selected[10]=false;
                    unselect(pos[10]);
                    pos[10]=0;
                }
                break;
        }

    }
    public void unselect(int val){
        if(selected[0] && pos[0]>=val) {
            pos[0]--;
            TextView txt=(TextView) findViewById(R.id.so_peito_superior_esquerdo_text);
            txt.setText(Integer.toString(pos[0]));
        }
        if(selected[1] && pos[1]>=val) {
            pos[1]--;
            TextView txt=(TextView) findViewById(R.id.so_peito_superior_direito_text);
            txt.setText(Integer.toString(pos[1]));
        }
        if(selected[2] && pos[2]>=val) {
            pos[2]--;
            TextView txt=(TextView) findViewById(R.id.so_peito_inferior_esquerdo_text);
            txt.setText(Integer.toString(pos[2]));
        }
        if(selected[3] && pos[3]>=val) {
            pos[3]--;
            TextView txt=(TextView) findViewById(R.id.so_peito_inferior_direito_text);
            txt.setText(Integer.toString(pos[3]));
        }
        if(selected[4] && pos[4]>=val) {
            pos[4]--;
            TextView txt=(TextView) findViewById(R.id.so_peito_pescoco_text);
            txt.setText(Integer.toString(pos[4]));
        }
        if(selected[5] && pos[5]>=val) {
            pos[5]--;
            TextView txt=(TextView) findViewById(R.id.so_lateral_esquerdo_text);
            txt.setText(Integer.toString(pos[5]));
        }
        if(selected[6] && pos[6]>=val) {
            pos[6]--;
            TextView txt=(TextView) findViewById(R.id.so_costas_superior_esquerdo_text);
            txt.setText(Integer.toString(pos[6]));
        }
        if(selected[7] && pos[7]>=val) {
            pos[7]--;
            TextView txt=(TextView) findViewById(R.id.so_costas_superior_direito_text);
            txt.setText(Integer.toString(pos[7]));
        }
        if(selected[8] && pos[8]>=val) {
            pos[8]--;
            TextView txt=(TextView) findViewById(R.id.so_costas_inferior_esquerdo_text);
            txt.setText(Integer.toString(pos[8]));
        }
        if(selected[9] && pos[9]>=val) {
            pos[9]--;
            TextView txt=(TextView) findViewById(R.id.so_costas_inferior_direito_text);
            txt.setText(Integer.toString(pos[9]));
        }
        if(selected[10] && pos[10]>=val){
            pos[10]--;
            TextView txt=(TextView) findViewById(R.id.so_lateral_direito_text);
            txt.setText(Integer.toString(pos[10]));
        }
    }

    public void done(View view){
        for(int val=1; val<=11; val++) {
            for (int i = 0; i < 11; i++) {
                if(pos[i]==val) {
                    orderdList.add(samples.get(i));
                }
            }
        }
        if(orderdList.size()==0){
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
        else {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result",orderdList);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }
}
