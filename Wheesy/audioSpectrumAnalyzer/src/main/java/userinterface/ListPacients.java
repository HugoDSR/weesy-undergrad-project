package userinterface;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import com.google.common.collect.BiMap;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import github.bewantbe.audio_analyzer_for_android.R;
import userinterface.XMLParser.XMLParserDate;
import userinterface.XMLParser.XMLParserRecording;

public class ListPacients extends Activity implements SearchView.OnQueryTextListener{

    static BiMap<Integer,String> listPatients;
    static BiMap<Integer,String> listDates;
    static List listPatientsShow;
    static List listDatesShow;
    private ListView lvp;
    private ListView lvd;
    private SearchView search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listpatient);

        listPatientsShow = new ArrayList<String>(listPatients.values());

        lvp = (ListView) findViewById(R.id.listViewPatients);
        lvd = (ListView) findViewById(R.id.listViewDates);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listPatientsShow); //public static List<String> pecas = new ArrayList<String>();
        lvp.setAdapter(arrayAdapter);

        lvp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int _position, long id) {
                v.setSelected(true);
                int patientID = listPatients.inverse().get(listPatientsShow.get(_position));

                try {
                    XMLParserDate dat = new XMLParserDate(LoginActivity.ID_CREDENTIALS, patientID);
                    listDates = dat.execute().get();

                    listDatesShow = new ArrayList<String>(listDates.values());

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ListPacients.this, android.R.layout.simple_list_item_1, listDatesShow); //public static List<String> pecas = new ArrayList<String>();
                    lvd.setAdapter(arrayAdapter);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            }


        });

        lvd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int _position, long id) {
                v.setSelected(true);
                int sampleID = listDates.inverse().get(listDatesShow.get(_position));

                try {
                    XMLParserRecording rat = new XMLParserRecording(sampleID);
                    PositionAvailable.listSamples = rat.execute().get();

                    Intent intent = new Intent(ListPacients.this, PositionAvailable.class);
                    startActivity(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            }


        });


        search = (SearchView) findViewById(R.id.searchPatient);
        lvp.setTextFilterEnabled(true);
        setupSearchView();
    }

    private void setupSearchView() {
        search.setIconifiedByDefault(false);
        search.setOnQueryTextListener(this);
        search.setSubmitButtonEnabled(true);
        search.setQueryHint("Search Here");
    }

    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            lvp.clearTextFilter();
        } else {
            lvp.setFilterText(newText.toString());
        }
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
