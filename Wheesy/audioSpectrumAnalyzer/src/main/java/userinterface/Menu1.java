package userinterface;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import github.bewantbe.audio_analyzer_for_android.MyPreferences;
import github.bewantbe.audio_analyzer_for_android.R;

public class Menu1 extends Activity {

    boolean redeUA = true;
    private static final int REQUEST_ENABLE_BT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuinicial);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            redeUA = extras.getBoolean("UA_DISPONIVEL");
            // and get whatever type user account id is
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void novaconsulta(View v){
        Intent intent = new Intent(Menu1.this, PatientData.class);
        intent.putExtra("UA_DISPONIVEL", redeUA);
        startActivity(intent);
    }

    public void listSamples(View v){
       // Intent intent = new Intent(Menu1.this, DataList.class);
        //startActivity(intent);

        if(!isNetworkAvailable()){
            Toast toast = Toast.makeText(getApplicationContext(),"Internet não disponivel: Não é possivel aceder a exames",Toast.LENGTH_LONG);
            toast.show();
        }else {

            Intent intent = new Intent(Menu1.this, PatientData.class);
            startActivity(intent);
        }
    }

    public void exames(View v){

        if(isNetworkAvailable()){
            if(redeUA) {
                Intent intent = new Intent(Menu1.this, ListPacients.class);
                startActivity(intent);
            }else{
                Toast toast = Toast.makeText(this, "Não se encontra na rede UA. Base de dados não acessivel.", Toast.LENGTH_LONG);
                toast.show();
            }

        }else{
            Toast toast = Toast.makeText(this, "Internet desligada. Base de dados não acessivel.", Toast.LENGTH_LONG);
            toast.show();
        }

    }

    public void buttonExit(View v){
        finish();
        System.exit(0);
    }
    public void buttonOptions(View v){
        Intent settings = new Intent(getBaseContext(), MyPreferences.class);
        startActivity(settings);
    }

    public void buttonBluetoothConnection(View v){

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent turnOnIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);

            Toast.makeText(getApplicationContext(),"Bluetooth turned on" ,
                    Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getApplicationContext(),"Bluetooth is already on",
                    Toast.LENGTH_LONG).show();
        }

    }

    public void naoimplementado(View v){
        Toast toast = Toast.makeText(getApplicationContext(),"Ainda nao foi implementado",Toast.LENGTH_LONG);
        toast.show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
