package userinterface;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.BiMap;

import java.util.ArrayList;
import java.util.List;

import github.bewantbe.audio_analyzer_for_android.AnalyzeActivity;
import github.bewantbe.audio_analyzer_for_android.R;

public class PositionAvailable extends Activity {

    static BiMap<String,String> listSamples;
    static List listPositionAvailable;
    static final String url = "http://deti-pei-vm3.ua.pt:8080/file-upload-1.0.0-SNAPSHOT/api/download/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.posicaoavailable);

        listPositionAvailable = new ArrayList<String>(listSamples.inverse().values());

        LinearLayout linearLayoutPai = (LinearLayout) findViewById(R.id.linearLayoutParent);

        for (int i = 0; i < listPositionAvailable.size(); i++) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            final float scale = getResources().getDisplayMetrics().density;
            linearLayout.setLayoutParams(new ViewGroup.LayoutParams((int) (105 * scale), (int) (170 * scale)));
            linearLayout.setPadding(5, 5, 5, 5);

            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(new ViewGroup.LayoutParams((int) (105 * scale), (int) (105 * scale)));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);

            TextView textView = new TextView(this);
            textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            textView.setGravity(Gravity.CENTER);

            if(listPositionAvailable.get(i).equals("peito_pescoco")) {
                imageView.setTag(R.drawable.peito_pescoco);
                imageView.setImageResource(R.drawable.peito_pescoco);
                textView.setText("Peito Pescoço");
            } else if(listPositionAvailable.get(i).equals("peito_superior_esquerdo")) {
                imageView.setTag(R.drawable.peito_superior_esquerdo);
                imageView.setImageResource(R.drawable.peito_superior_esquerdo);
                textView.setText("Peito Superior Esquerdo");
            } else if(listPositionAvailable.get(i).equals("peito_superior_direito")) {
                imageView.setTag(R.drawable.peito_superior_direito);
                imageView.setImageResource(R.drawable.peito_superior_direito);
                textView.setText("Peito Superior Direito");
            } else if(listPositionAvailable.get(i).equals("peito_inferior_esquerdo")) {
                imageView.setTag(R.drawable.peito_inferior_esquerdo);
                imageView.setImageResource(R.drawable.peito_inferior_esquerdo);
                textView.setText("Peito Inferior Esquerdo");
            } else if(listPositionAvailable.get(i).equals("peito_inferior_direito")) {
                imageView.setTag(R.drawable.peito_inferior_direito);
                imageView.setImageResource(R.drawable.peito_inferior_direito);
                textView.setText("Peito Inferior Direito");
            } else if(listPositionAvailable.get(i).equals("lateral_esquerdo")) {
                imageView.setTag(R.drawable.lateral_esquerdo);
                imageView.setImageResource(R.drawable.lateral_esquerdo);
                textView.setText("Lateral Esquerdo");
            } else if(listPositionAvailable.get(i).equals("costas_superior_esquerdo")) {
                imageView.setTag(R.drawable.costas_superior_esquerdo);
                imageView.setImageResource(R.drawable.costas_superior_esquerdo);
                textView.setText("Costas Superior Esquerdo");
            } else if(listPositionAvailable.get(i).equals("costas_superior_direito")) {
                imageView.setTag(R.drawable.costas_superior_direito);
                imageView.setImageResource(R.drawable.costas_superior_direito);
                textView.setText("Costas Superior Direito");
            } else if(listPositionAvailable.get(i).equals("costas_inferior_esquerdo")) {
                imageView.setTag(R.drawable.costas_inferior_esquerdo);
                imageView.setImageResource(R.drawable.costas_inferior_esquerdo);
                textView.setText("Costas Inferior Esquerdo");
            } else if(listPositionAvailable.get(i).equals("costas_inferior_direito")) {
                imageView.setTag(R.drawable.costas_inferior_direito);
                imageView.setImageResource(R.drawable.costas_inferior_direito);
                textView.setText("Costas Inferior Direito");
            } else if(listPositionAvailable.get(i).equals("lateral_direito")) {
                imageView.setTag(R.drawable.lateral_direito);
                imageView.setImageResource(R.drawable.lateral_direito);
                textView.setText("Lateral Direito");
            }
            imageView.setClickable(true);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImageView image = (ImageView) v;
                    if ((Integer) image.getTag() == R.drawable.peito_pescoco) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("peito_pescoco"), Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(PositionAvailable.this, AnalyzeActivity.class);

                        ArrayList<String> local = new ArrayList<String>();

                        local.add("peito_pescoco");

                        intent.putExtra("EXTRA_LOCALLIST", local);
                        intent.putExtra("UA_DISPONIVEL", true);
                        intent.putExtra("EXTRA_NOMECLIENT", "teste");
                        startActivity(intent);

                    } else if((Integer) image.getTag() == R.drawable.peito_superior_esquerdo) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("peito_superior_esquerdo"),Toast.LENGTH_LONG).show();
                    } else if((Integer) image.getTag() == R.drawable.peito_superior_direito) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("peito_superior_direito"),Toast.LENGTH_LONG).show();
                    } else if((Integer) image.getTag() == R.drawable.peito_inferior_esquerdo) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("peito_inferior_esquerdo"),Toast.LENGTH_LONG).show();
                    } else if((Integer) image.getTag() == R.drawable.peito_inferior_direito) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("peito_inferior_direito"),Toast.LENGTH_LONG).show();
                    } else if((Integer) image.getTag() == R.drawable.lateral_esquerdo) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("lateral_esquerdo"),Toast.LENGTH_LONG).show();
                    } else if((Integer) image.getTag() == R.drawable.costas_superior_esquerdo) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("costas_superior_esquerdo"),Toast.LENGTH_LONG).show();
                    } else if((Integer) image.getTag() == R.drawable.costas_superior_direito) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("costas_superior_direito"),Toast.LENGTH_LONG).show();
                    } else if((Integer) image.getTag() == R.drawable.costas_inferior_esquerdo) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("costas_inferior_esquerdo"),Toast.LENGTH_LONG).show();
                    } else if((Integer) image.getTag() == R.drawable.costas_inferior_direito) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("costas_inferior_direito"),Toast.LENGTH_LONG).show();
                    } else if((Integer) image.getTag() == R.drawable.lateral_direito) {
                        Toast.makeText(PositionAvailable.this, url + listSamples.get("lateral_direito"),Toast.LENGTH_LONG).show();
                    }
                }
            });

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            textView.setLayoutParams(params);
            textView.setTextColor(Color.BLACK);

            linearLayout.addView(imageView);
            linearLayout.addView(textView);

            linearLayoutPai.addView(linearLayout);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void teste(View v) {
        Toast.makeText(this, "Descarregar tudo!",Toast.LENGTH_LONG).show();
    }
}
