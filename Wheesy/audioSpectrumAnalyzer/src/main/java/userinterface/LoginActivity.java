package userinterface;

/**
 * Created by Rui on 07/04/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import databaseClient.GetClient;
import github.bewantbe.audio_analyzer_for_android.AnalyzeActivity;
import github.bewantbe.audio_analyzer_for_android.AnalyzeActivityFromWav;
import github.bewantbe.audio_analyzer_for_android.R;
import userinterface.XMLParser.XMLParserLogin;
import userinterface.XMLParser.XMLParserPatients;


/**
 * Android login screen Activity
 */
public class LoginActivity extends Activity /*implements LoaderCallbacks<Cursor>*/ {

    static int ID_CREDENTIALS = 0;

    //private UserLoginTask userLoginTask = null;
    private View loginFormView;
    private View progressView;
    private AutoCompleteTextView emailTextView;
    private EditText passwordTextView;
    private TextView signUpTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        }

    public void login (View v){
        EditText email = (EditText) findViewById(R.id.email);
        EditText pass = (EditText) findViewById(R.id.password);

        if(isNetworkAvailable()) {

            XMLParserLogin xml = new XMLParserLogin(email.getText().toString(), pass.getText().toString());
            int login;

            try {
                login = xml.execute().get();

                if (login == -1){
                    Toast toast = Toast.makeText(this, "Rede UA não acessivel. Base de dados indisponível", Toast.LENGTH_LONG);
                    toast.show();

                    Intent intent = new Intent(this, Menu1.class);
                    intent.putExtra("UA_DISPONIVEL", false);
                    startActivity(intent);
                }
                else if (login != 0) {
                    ID_CREDENTIALS = login;
                    XMLParserPatients pat = new XMLParserPatients(ID_CREDENTIALS);
                    ListPacients.listPatients = pat.execute().get();

                    Intent intent = new Intent(this, Menu1.class);
                    startActivity(intent);
                } else {
                    Toast toast = Toast.makeText(this, "Login Inválido", Toast.LENGTH_LONG);
                    toast.show();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }


        }else{

            Toast toast = Toast.makeText(this, "Internet desligada. Base de dados não acessivel.", Toast.LENGTH_LONG);
            toast.show();


            Intent intent = new Intent(this, Menu1.class);
            intent.putExtra("UA_DISPONIVEL", false);
            startActivity(intent);

        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void demo(View view) {
        AnalyzeActivity.DEMO_MODE = true;

        ArrayList<String> local = new ArrayList<String>();

        local.add("peito_pescoco");
        local.add("peito_superior_esquerdo");
        local.add("peito_superior_direito");
        local.add("peito_inferior_esquerdo");
        local.add("peito_inferior_direito");
        local.add("lateral_esquerdo");
        local.add("costas_superior_esquerdo");
        local.add("costas_superior_direito");
        local.add("costas_inferior_esquerdo");
        local.add("costas_inferior_direito");
        local.add("lateral_direito");

        Intent intent = new Intent(this, AnalyzeActivity.class);
        if (isNetworkAvailable()) {
            intent.putExtra("UA_DISPONIVEL", true);
        } else {
            intent.putExtra("UA_DISPONIVEL", false);
        }
        intent.putExtra("EXTRA_NOMECLIENT", "Demo");
        intent.putExtra("EXTRA_LOCALLIST", local);
        startActivity(intent);
    }
}