package userinterface.XMLParser;

import android.os.AsyncTask;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import userinterface.Patient;

/**
 * Created by tiagoteixeira on 16-05-2016.
 */
public class XMLParserGetPatient extends AsyncTask<String, Void, Patient> {

    URL url;
    Patient patient;
    int idPatient;


    public XMLParserGetPatient(int idPatient) {
        this.idPatient = idPatient;
        patient = new Patient(idPatient);
    }

    @Override
    protected Patient doInBackground(String... objects) {
        try {
            url = new URL("http://deti-pei-vm3.ua.pt:8080/lasttry-1.0-SNAPSHOT/webresources/com.mycompany.lasttry.pacient/"+idPatient);

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(getInputStream(url), "UTF-8");

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (xpp.getName().equalsIgnoreCase("address")) {
                        patient.setAddress(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("birthdate")) {
                        patient.setDataNascimento(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("height")) {
                        patient.setAltura(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("name")) {
                        patient.setNome(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("phone")) {
                        patient.setTelefone(Integer.parseInt(xpp.nextText()));
                    } else if (xpp.getName().equalsIgnoreCase("sex")) {
                        patient.setSexo(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("weight")) {
                        patient.setPeso(Integer.parseInt(xpp.nextText()));
                    }
                }
                eventType = xpp.next();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return patient;
    }

    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }

}