package userinterface.XMLParser;


import android.os.AsyncTask;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class XMLParserRecording extends AsyncTask<String, Void, BiMap<String,String>> {

    URL url;
    BiMap<String,String> samples = HashBiMap.create();
    int idSample;

    public XMLParserRecording(int idSample) {
        this.idSample = idSample;
    }

    @Override
    protected BiMap<String,String> doInBackground(String... objects) {
        try {
            url = new URL("http://deti-pei-vm3.ua.pt:8080/lasttry-1.0-SNAPSHOT/webresources/com.mycompany.lasttry.recording/"+idSample+"/recordings");
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(getInputStream(url), "UTF-8");

            String dataLocation = "";
            String position = "";

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (xpp.getName().equalsIgnoreCase("datalocation")) {
                        dataLocation = xpp.nextText();
                    } else if (xpp.getName().equalsIgnoreCase("position")) {
                        position = xpp.nextText();
                        samples.put(position, dataLocation + "/" + position);
                    }
                }
                eventType = xpp.next();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return samples;
    }

    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }

}

