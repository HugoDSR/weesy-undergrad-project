package userinterface.XMLParser;

import android.os.AsyncTask;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;


public class XMLParserPatients extends AsyncTask<String, Void, BiMap<Integer,String>> {

    URL url;
    BiMap<Integer,String> patients = HashBiMap.create();

    int idDoctor;


    public XMLParserPatients(int idDoctor) {
        this.idDoctor = idDoctor;

    }

    @Override
    protected BiMap<Integer,String> doInBackground(String... objects) {
        try {
            url = new URL("http://deti-pei-vm3.ua.pt:8080/lasttry-1.0-SNAPSHOT/webresources/com.mycompany.lasttry.samples/"+ idDoctor + "/pacientes");

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(getInputStream(url), "UTF-8");
            
            int idPat = 0;

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (xpp.getName().equalsIgnoreCase("id")) {
                        idPat = Integer.parseInt(xpp.nextText());
                    } else if (xpp.getName().equalsIgnoreCase("name")) {
                            patients.put(idPat,xpp.nextText());
                    }
                }
                eventType = xpp.next();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return patients;
    }

    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }

}

