package userinterface.XMLParser;

import android.os.AsyncTask;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;


public class XMLParserLogin extends AsyncTask<String, Void, Integer> {

    URL url;
    String mail,pass;

    public XMLParserLogin(String mail, String pass) {
        this.mail = mail;
        this.pass = pass;
    }

    @Override
    protected Integer doInBackground(String... restURL) {
        int login = 0;
        int partial = 0;
        try {
            url = new URL("http://deti-pei-vm3.ua.pt:8080/lasttry-1.0-SNAPSHOT/webresources/com.mycompany.lasttry.doctor");

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(getInputStream(url), "UTF-8");

            boolean insideItem = false;

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (xpp.getName().equalsIgnoreCase("email")) {
                        if (xpp.nextText().equals(mail)) {
                            insideItem = true;
                        }
                    } else if (xpp.getName().equalsIgnoreCase("id")) {
                        partial = Integer.parseInt(xpp.nextText());

                    } else if (xpp.getName().equalsIgnoreCase("password")) {
                        if (insideItem && xpp.nextText().equals(pass)) {
                            login = partial;
                            return login;
                        }

                    } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                        insideItem = false;
                    }
                }

                eventType = xpp.next();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            login = -1;
        }

        return login;
    }


    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }


}